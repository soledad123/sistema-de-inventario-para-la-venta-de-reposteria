import React,{Component} from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import  routes  from  '/imports/routes/Routes'
import Principal from '/imports/view/principal/Principal'
import SwitchRoutes from '/imports/routes/SwitchRoutes'
import {BrowserRouter,Route,Switch,Redirect} from 'react-router-dom';
  
class App extends Component {
    render() {
        return (
            <BrowserRouter>
            <Switch>
             {/* <Route exact path="/" component={Principal}/>*/}
              <Route exact path="/">
                <Redirect to="/bienvenido/principal"/>
              </Route>
              <Route exact path="/bienvenido">
                <Redirect to="/bienvenido/principal"/>
              </Route>
              {
                routes.map((route,i)=>{
                 return <SwitchRoutes key={i} {...route}/>       
                })
              }
              </Switch>
            </BrowserRouter>
        )
    }
}




Meteor.startup(() => {
  render(<App/>, document.getElementById('Aqui_se_dibujara_mi_sistema'));
});
