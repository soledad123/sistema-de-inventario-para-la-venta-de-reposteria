import React, { Component } from 'react'

import {Link,Switch} from 'react-router-dom'
import SwitchRoutes from '/imports/routes/SwitchRoutes'

  export default  class Principal extends Component {
   componentDidMount(){
    import'/imports/assets/principal/css'
    import'/imports/assets/principal/js'
    import desingPrincipal from'/imports/assets/principal/js/main'
     desingPrincipal()
   }
    render() {
        const {routes}= this.props
        return (
    <div>
         <div id="preloder">
        <div className="loader"></div>


        </div>
         <div className="offcanvas-menu-overlay"></div>
    <div className="offcanvas-menu-wrapper">
        <div className="offcanvas__cart">
            <div className="offcanvas__cart__links">
                <a href="#" className="search-switch"><img src="principal/img/icon/search.png" alt=""/></a>
                <a href="#"><img src="img/icon/heart.png" alt=""/></a>
            </div>
            <div className="offcanvas__cart__item">
                <a href="#"><img src="principal/img/icon/cart.png" alt=""/> <span>0</span></a>
                <div className="cart__price">Cart: <span>$0.00</span></div>
            </div>
        </div>
        <div className="offcanvas__logo">
            <a href=".//"><img src="principal/img/logo.png" alt=""/></a>
        </div>
        <div id="mobile-menu-wrap"></div>
        <div className="offcanvas__option">
            <ul>
                <li>USD <span className="arrow_carrot-down"></span>
                    <ul>
                        <li>EUR</li>
                        <li>USD</li>
                    </ul>
                </li>
                <li>ENG <span className="arrow_carrot-down"></span>
                    <ul>
                    <li className="button-header">
                             <Link to="/bienvenido/login" className="btn3">
                                 Iniciar Sesion
                             </Link>
                            </li>
                    </ul>
                </li>
                <li><a href="#">Sign in</a> <span className="arrow_carrot-down"></span></li>
                
            </ul>
        </div>
        </div> 

    <header className="header">
        <div className="header__top">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="header__top__inner">
                            <div className="header__top__left">
                                <ul>
                                    <li>USD <span className="arrow_carrot-down"></span>
                                        <ul>
                                            <li>EUR</li>
                                            <li>USD</li>
                                        </ul>
                                    </li>
                                    <li>ENG <span className="arrow_carrot-down"></span>
                                        <ul>
                                    
                                
                            
                                            <li>Spanish</li>
                                            
                                            
                                        </ul>
                                    </li>
                                    
                                    <li className="button-header">
                             <Link to="/bienvenido/login" className="btn3">
                                 Iniciar Sesion
                             </Link>
                            </li>
                                </ul>
                            </div>
                            <div className="header__logo">
                                <a href=".//"><img src="principal/img/logo.png" alt=""/></a>
                            </div>
                            <div className="header__top__right">
                                <div className="header__top__right__links">
                                    <a href="#" className="search-switch"><img src="principal/img/icon/search.png" alt=""/></a>
                                    <a href="#"><img src="principal/img/icon/heart.png" alt=""/></a>
                                </div>
                                <div className="header__top__right__cart">
                                    <a href="#"><img src="principal/img/icon/cart.png" alt=""/> <span>0</span></a>
                                    <div className="cart__price">Cart: <span>$0.00</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="canvas__open"><i className="fa fa-bars"></i></div>
            </div>
        </div>
        <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    <nav className="header__menu mobile-menu">
                        <ul>
                            <li className="active"><a href=".//">Home</a></li>
                            <li><a href="/">About</a></li>
                            <li><a href="/">Shop</a></li>
                            <li><a href="#">Pages</a>
                                <ul className="dropdown">
                                    <li><a href="./shop-details.html">Shop Details</a></li>
                                    <li><a href="./shoping-cart.html">Shoping Cart</a></li>
                                    <li><a href="./checkout.html">Check Out</a></li>
                                    <li><a href="./wisslist.html">Wisslist</a></li>
                                    <li><a href="./Class.html">Class</a></li>
                                    <li><a href="./blog-details.html">Blog Details</a></li>
                                </ul>
                            </li>
                            <li><a href="/">Blog</a></li>
                            <li><a href="/">Contact</a></li>
                            
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <main>
       <Switch>
             {
             routes.map((route,i)=>{
                return <SwitchRoutes key={i} {...route} />
                
            })
        }
         </Switch>
                
    </main>


    <footer className="footer set-bg" data-setbg="principal/img/footer-bg.jpg">
        <div className="container">
            <div className="row">
                <div className="col-lg-4 col-md-6 col-sm-6">
                    <div className="footer__widget">
                        <h6>WORKING HOURS</h6>
                        <ul>
                            <li>Monday - Friday: 08:00 am – 08:30 pm</li>
                            <li>Saturday: 10:00 am – 16:30 pm</li>
                            <li>Sunday: 10:00 am – 16:30 pm</li>
                        </ul>
                    </div>
                </div>
                <div className="col-lg-4 col-md-6 col-sm-6">
                    <div className="footer__about">
                        <div className="footer__logo">
                            <a href="#"><img src="principal/img/footer-logo.png" alt=""/></a>
                        </div>
                        <p>Lorem ipsum dolor amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore dolore magna aliqua.</p>
                        <div className="footer__social">
                            <a href="#"><i className="fa fa-facebook"></i></a>
                            <a href="#"><i className="fa fa-twitter"></i></a>
                            <a href="#"><i className="fa fa-instagram"></i></a>
                            <a href="#"><i className="fa fa-youtube-play"></i></a>
                        </div>
                    </div>
                </div>
                <div className="col-lg-4 col-md-6 col-sm-6">
                    <div className="footer__newslatter">
                        <h6>Subscribe</h6>
                        <p>Get latest updates and offers.</p>
                        <form action="#">
                            <input type="text" placeholder="Email"/>
                            <button type="submit"><i className="fa fa-send-o"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div className="copyright">
            <div className="container">
                <div className="row">
                    <div className="col-lg-7">
                        <p className="copyright__text text-white">
                          Copyright &copy;2021 All rights reserved | This template is made with <i className="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                          
                      </p>
                  </div>
                  <div className="col-lg-5">
                    <div className="copyright__widget">
                        <ul>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                            <li><a href="#">Site Map</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </footer>
<div className="search-model">
    <div className="h-100 d-flex align-items-center justify-content-center">
        <div className="search-close-switch">+</div>
        <form className="search-model-form">
            <input type="text" id="search-input" placeholder="Search here....."/>
        </form>
    </div>
</div>
            
  </div>             
        )
    }
}
