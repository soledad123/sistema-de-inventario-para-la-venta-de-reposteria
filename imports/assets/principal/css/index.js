import './bootstrap.min'
import './slicknav.min'
import './barfiller'
import './elegant-icons'
import './flaticon'
import './font-awesome.min'
import './magnific-popup'
import './nice-select'
import './owl.carousel.min'
import './style'