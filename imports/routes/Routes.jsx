import React, {Component }from 'react'
import Principal from '/imports/view/principal/Principal'
import Main from '/imports/view/principal/Main'
import Login from '/imports/view/auth/Login'
import Layout from '/imports/view/layout/Layout'
import Home from '/imports/view/home/Home'


import {useLocation}from "react-router-dom"

function NoMatch() {
    let location =useLocation();
    return (
        <div>
            <h3>
                La pagina solicitada no existe <code>{location.pathname}</code>
            </h3>
        </div>
    );
}
const routes =[
    {
        path:'/bienvenido',                                                                                                                                                                                                                                                       
        component:Principal,
        routes:[
            {
                path:'/bienvenido/principal',
                component:Main
        
        },
        {
            path:'/bienvenido/login',
            component:Login
        }
        ]

    },
    {
        path:'/dashboard',
        component:Layout,
        routes: [
            {
                path:"/dashboard/home",
        component:Home, 
            },
            {
                path:"/dashboard/home2",
                component:Login, 
            }


    ]
},
{
    path:'*',
    component:NoMatch,
}
]

export default routes